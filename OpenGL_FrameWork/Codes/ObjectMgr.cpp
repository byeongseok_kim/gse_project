#include "pch.h"
#include "..\Headers\ObjectMgr.h"
#include "Obj.h"
#include "Transform.h"

_IMPLEMENT_SINGLETON(CObjectMgr)
CObjectMgr::CObjectMgr()
{
}

CObjectMgr::~CObjectMgr()
{
	Release();
}

void CObjectMgr::Add_Object(CObj* pObj, OBJECT eID)
{
	m_vecObj[eID].push_back(pObj);
}

void CObjectMgr::Update(const _float& fTimeDelta)
{
	for (int i = 0; i < OBJECT_END; ++i)
	{
		auto iter_begin = m_vecObj[i].begin();

		for (; iter_begin != m_vecObj[i].end();)
		{
			int iEvent = (*iter_begin)->Update(fTimeDelta);


			if (1 == iEvent)
			{
				SafeDelete(*iter_begin);
				iter_begin = m_vecObj[i].erase(iter_begin);
			}
			else
				++iter_begin;
		}
	}




}

void CObjectMgr::LateUpdate(const _float& fTimeDelta)
{
	for (int i = 0; i < OBJECT_END; ++i)
	{
		for (auto& iter : m_vecObj[i])
		{
			iter->Late_Update(fTimeDelta);
			LAYER_ID eID = iter->Get_ID();
			m_LstRender[eID].push_back(iter);
		}
	}

	CCollisionMgr::GetInstance()->Check_Collision(m_vecObj[OBJECT_BULLET], m_vecObj[OBJECT_MONSTER],3.f);
	CCollisionMgr::GetInstance()->Check_Collision(m_vecObj[OBJECT_MONSTER_BULLET], m_vecObj[OBJECT_PLAYER], 5.f);
	CCollisionMgr::GetInstance()->Check_Collision(m_vecObj[OBJECT_ATTACKRANGE], m_vecObj[OBJECT_PLAYER]);

}

void CObjectMgr::Render()
{

	for (auto& iter : m_LstRender)
	{
		
		iter.sort([](auto& pDst, auto& pSrc){
				return pDst->GetTransform()->Get_StateInfo(STATE_POSITION)->y > pSrc->GetTransform()->Get_StateInfo(STATE_POSITION)->y;});
		for (auto& pObject : iter)
			pObject->Render();

		iter.clear();
	}
}

void CObjectMgr::Release()
{
	for (int i = 0; i < OBJECT_END; ++i)
	{
		for_each(m_vecObj[i].begin(), m_vecObj[i].end(), SafeDelete<CObj*>);
		m_vecObj[i].clear();
	}
}

void CObjectMgr::Group_Release(OBJECT eID)
{
	for_each(m_vecObj[eID].begin(), m_vecObj[eID].end(), SafeDelete<CObj*>);
	m_vecObj[eID].clear();
}

