#include "pch.h"
#include "..\Headers\Buffer_RectTex.h"
#include "Shader.h"
#include "Texture.h"
CBuffer_RectTex::CBuffer_RectTex()
{
}


CBuffer_RectTex::~CBuffer_RectTex()
{
}

void CBuffer_RectTex::Init()
{
	m_iNumVertices = 4;
	m_iNumPolygons = 2;

	m_vecVertex.resize(m_iNumVertices);

	m_vecVertex[0].vPos = { -0.5f, -0.5f, 0.f };
	m_vecVertex[0].vNormal = { };
	m_vecVertex[0].vTexUV = { 0.f, 1.f };

	m_vecVertex[1].vPos = { 0.5f, -0.5f, 0.f };
	m_vecVertex[1].vNormal = { };
	m_vecVertex[1].vTexUV = { 1.f, 1.f};

	m_vecVertex[2].vPos = { 0.5f, 0.5f, 0.f };
	m_vecVertex[2].vNormal = { };
	m_vecVertex[2].vTexUV = { 1.f, 0.f};

	m_vecVertex[3].vPos = { -0.5f, 0.5f, 0.f };
	m_vecVertex[3].vNormal = { };
	m_vecVertex[3].vTexUV = { 0.f, 0.f };

	m_vecIndices.resize(6);
	m_vecIndices =
	{
		0, 1, 3,
		1, 2, 3
	};
	for (int i = 0; i < m_iNumPolygons*3;)
	{
		vec3 vSour, vDest, vNormal;
		//����
		vSour = m_vecVertex[m_vecIndices[i + 1]].vPos - m_vecVertex[m_vecIndices[i]].vPos;
		vDest = m_vecVertex[m_vecIndices[i + 2]].vPos - m_vecVertex[m_vecIndices[i]].vPos;
		vNormal = normalize(cross(vSour, vDest));

		m_vecVertex[m_vecIndices[i]].vNormal += vNormal;
		m_vecVertex[m_vecIndices[i + 1]].vNormal += vNormal;
		m_vecVertex[m_vecIndices[i + 2]].vNormal += vNormal;
		++++++i;

		//���
		vSour = m_vecVertex[m_vecIndices[i + 2]].vPos - m_vecVertex[m_vecIndices[i + 1]].vPos;
		vDest = m_vecVertex[m_vecIndices[i]].vPos - m_vecVertex[m_vecIndices[i + 1]].vPos;
		vNormal = normalize(cross(vSour, vDest));

		m_vecVertex[m_vecIndices[i]].vNormal += vNormal;
		m_vecVertex[m_vecIndices[i + 1]].vNormal += vNormal;
		m_vecVertex[m_vecIndices[i + 2]].vNormal += vNormal;
		++++++i;


	}
	glGenVertexArrays(1, &VAO);

	glGenBuffers(1, &VBO_Pos);
	glGenBuffers(1, &VBO_NORMAL);
	glGenBuffers(1, &VBO_UV);
	glGenBuffers(1, &EBO);
	glGenTextures(1, &m_iTexture);

	_uint TextureLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "outTexture");
	glUniform1i(TextureLocation, 0);

	glBindTexture(GL_TEXTURE_2D, m_iTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);


	glBindVertexArray(VAO);


	glBindBuffer(GL_ARRAY_BUFFER, VBO_Pos);
	glBufferData(GL_ARRAY_BUFFER, m_vecVertex.size() * sizeof(VTXUV), &m_vecVertex.front(), GL_STATIC_DRAW);
	_int	iAttribute = glGetAttribLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "vPos");
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VTXUV), 0);
	glEnableVertexAttribArray(iAttribute);

	glBindBuffer(GL_ARRAY_BUFFER, VBO_NORMAL);
	glBufferData(GL_ARRAY_BUFFER, m_vecVertex.size() * sizeof(VTXUV), &m_vecVertex.front(), GL_STATIC_DRAW);
	iAttribute = glGetAttribLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "vNormal");
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VTXUV), (void*)offsetof(VTXUV, vNormal));
	glEnableVertexAttribArray(iAttribute);


	glBindBuffer(GL_ARRAY_BUFFER, VBO_UV);
	glBufferData(GL_ARRAY_BUFFER, m_vecVertex.size() * sizeof(VTXUV), &m_vecVertex.front(), GL_STATIC_DRAW);
	iAttribute = glGetAttribLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "vTexUV");
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VTXUV), (void*)offsetof(VTXUV, vTexUV));
	glEnableVertexAttribArray(iAttribute);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_vecIndices.size() * sizeof(_uint), &m_vecIndices.front(), GL_STATIC_DRAW);

}

void CBuffer_RectTex::Update()
{
} 

void CBuffer_RectTex::Render()
{

	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, m_vecIndices.size(), GL_UNSIGNED_INT, (void*)0);
}

void CBuffer_RectTex::Release()
{
}
