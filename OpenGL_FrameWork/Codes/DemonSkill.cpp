#include "pch.h"
#include "DemonSkill.h"
#include "Buffer_RectTex.h"
#include "Shader.h"
#include "Transform.h"
#include "Texture.h"
#include "FireHit.h"
CDemonSkill::CDemonSkill()
{
}

CDemonSkill::~CDemonSkill()
{
	Release();
}

HRESULT CDemonSkill::Init()
{
	return S_OK;
}

HRESULT CDemonSkill::Init(vec3 vPos, vec3 vSize, _int iIdx)
{
	m_pBuffer = new CBuffer_RectTex;
	m_pTransform = new CTransform;
	m_pBuffer->Init();
	m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	m_pTransform->SetUp_Speed(2.f, radians(5.f));
	m_eCurDir = (DIR)iIdx;
	m_eID = ID_EFFECT;



	if (m_eCurDir == DIR_UP)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"FireBolt0");
	}
	else if (m_eCurDir == DIR_DOWN)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"FireBolt8");
	}
	else if (m_eCurDir == DIR_LEFT)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"FireBolt12");
	}
	else if (m_eCurDir == DIR_RIGHT)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"FireBolt4");
	}
	m_tFrame = { 0, (_float)m_vecTexture.size() };


	return S_OK;
}

int CDemonSkill::Update(const _float& fTimeDelta)
{
	CSoundMgr::GetInstance()->PlayOnce(L"F_FireBall.wav", CSoundMgr::SOUND_EFFECT);
	m_fLifeTime += fTimeDelta / 1000.f;
	if (m_fLifeTime >= 2.f)
		m_IsDead = true;

	if (m_IsHit)
	{
		m_IsDead = true;
		CObj* pObj = CAbstractFactory<CFireHit>::CreateObj(*m_pTransform->Get_StateInfo(STATE_POSITION), vec3(1.f, 1.f, 1.f), 0);
		if (nullptr == pObj)
			return 0;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_EFFECT);
	}
	else
	{
		if (m_eCurDir == DIR_UP)
		{
			m_pTransform->Go_Up(fTimeDelta / 1000.f);
		}
		else if (m_eCurDir == DIR_DOWN)
		{
			m_pTransform->Go_Down(fTimeDelta / 1000.f);
		}
		else if (m_eCurDir == DIR_LEFT)
		{
			m_pTransform->Go_Left(fTimeDelta / 1000.f);
		}
		else if (m_eCurDir == DIR_RIGHT)
		{
			m_pTransform->Go_Right(fTimeDelta / 1000.f);
		}

	}


	CObj::MoveFrame(fTimeDelta / 1000.f);


	if (m_IsDead)
		return 1;


	if (m_IsDead)
		return 1;
	return 0;
}

void CDemonSkill::Late_Update(const _float& fTimeDelta)
{
}

void CDemonSkill::Render()
{
	glUseProgram(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum());

	mat4 matView = CCameraMgr::GetInstance()->Get_ViewMatrix();
	mat4 matProj = CCameraMgr::GetInstance()->Get_ProjMatrix();
	_uint WorldLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matWorld");
	glUniformMatrix4fv(WorldLocation, 1, GL_FALSE, value_ptr(m_pTransform->GetMatrix()));

	_uint ViewLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matView");
	glUniformMatrix4fv(ViewLocation, 1, GL_FALSE, &matView[0][0]);

	_uint ProjLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matProj");
	glUniformMatrix4fv(ProjLocation, 1, GL_FALSE, &matProj[0][0]);

	_uint TextureLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "outTexture");
	glUniform1i(TextureLocation, 0);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_vecTexture[m_tFrame.fFrame]);


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	m_pBuffer->Render();


	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void CDemonSkill::Release()
{
	SafeDelete(m_pBuffer);
	SafeDelete(m_pTransform);
}
