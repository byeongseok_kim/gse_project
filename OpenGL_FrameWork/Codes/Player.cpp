#include "pch.h"
#include "Player.h"
#include "Buffer_RectTex.h"
#include "Shader.h"
#include "Transform.h"
#include "Texture.h"

#include "IceBolt.h"
#include "Cast.h"
#include "Armor.h"
#include "IceOrb.h"


CPlayer::CPlayer()
{
}

CPlayer::~CPlayer()
{
	Release();
}

HRESULT CPlayer::Init()
{
	m_pBuffer = new CBuffer_RectTex;
	m_pTransform = new CTransform;
	m_pBuffer->Init();
	m_pTransform->SetUp_Speed(1.f, radians(5.f));
	m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Idle8");
	m_tFrame = {0, (_float)m_vecTexture.size()};
	m_ePlayerState = IDLE;
	m_eCurDir = DIR_DOWN; 
	m_eID = ID_GAMEOBJECT;

	m_tInfo.iHp = 10;
	return S_OK;
}

int CPlayer::Update(const _float& fTimeDelta)
{

	if (m_tInfo.iHp <= 0)
	{
		_int i = 0;
	}
	if(m_ePlayerState == IDLE)
		CObj::MoveFrame(fTimeDelta / 1000.f);
	else
	{
		if (!CObj::MoveFrame_Once(fTimeDelta / 1000.f))
		{
			if (m_eCurDir == DIR_UP)
				m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Idle0");
			else if (m_eCurDir == DIR_DOWN)
				m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Idle8");
			else if (m_eCurDir == DIR_LEFT)
				m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Idle11");
			else if (m_eCurDir == DIR_RIGHT)
				m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Idle3");
			m_tFrame = { 0, (_float)m_vecTexture.size() };
			m_ePlayerState = IDLE;
		}
	}


	if (!m_IsHit)
	{
		// Movement
		if (CKeyMgr::GetInstance()->KeyPressing(KEY_UP))
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Walk0");
			m_pTransform->Go_Up(fTimeDelta / 1000.f);
			m_eCurDir = DIR_UP;
			if (m_vecTexture.size() < m_tFrame.fFrame)
				m_tFrame.fFrame = 0;
			m_tFrame.fMax = (_float)m_vecTexture.size();
		}
		else if (CKeyMgr::GetInstance()->KeyPressing(KEY_DOWN))
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Walk8");
			m_pTransform->Go_Down(fTimeDelta / 1000.f);
			m_eCurDir = DIR_DOWN;
			if (m_vecTexture.size() < m_tFrame.fFrame)
				m_tFrame.fFrame = 0;
			m_tFrame.fMax = (_float)m_vecTexture.size();
		}
		else if (CKeyMgr::GetInstance()->KeyPressing(KEY_LEFT))
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Walk11");
			m_pTransform->Go_Left(fTimeDelta / 1000.f);
			m_eCurDir = DIR_LEFT;
			if (m_vecTexture.size() < m_tFrame.fFrame)
				m_tFrame.fFrame = 0;
			m_tFrame.fMax = (_float)m_vecTexture.size();
		}
		else if (CKeyMgr::GetInstance()->KeyPressing(KEY_RIGHT))
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Walk3");
			m_pTransform->Go_Right(fTimeDelta / 1000.f);
			m_eCurDir = DIR_RIGHT;
			if (m_vecTexture.size() < m_tFrame.fFrame)
				m_tFrame.fFrame = 0;
			m_tFrame.fMax = (_float)m_vecTexture.size();
		}

		if (CKeyMgr::GetInstance()->KeyUp(KEY_UP))
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Idle0");
			m_tFrame = { 0, (_float)m_vecTexture.size() };
		}
		else if (CKeyMgr::GetInstance()->KeyUp(KEY_DOWN))
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Idle8");
			m_tFrame = { 0, (_float)m_vecTexture.size() };
		}
		else if (CKeyMgr::GetInstance()->KeyUp(KEY_LEFT))
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Idle11");
			m_tFrame = { 0, (_float)m_vecTexture.size() };
		}
		else if (CKeyMgr::GetInstance()->KeyUp(KEY_RIGHT))
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Idle3");
			m_tFrame = { 0, (_float)m_vecTexture.size() };
		}
	}


	//Skill
	if (CKeyMgr::GetInstance()->KeyDown(KEY_Z))
	{
		CSoundMgr::GetInstance()->PlayOnce(L"C_FrostNova.wav", CSoundMgr::SOUND_PLAYER);
 		m_ePlayerState = SKILL;
		if (m_eCurDir == DIR_UP)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Skill0");
		else if (m_eCurDir == DIR_DOWN)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Skill8");
		else if (m_eCurDir == DIR_LEFT)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Skill11");
		else if (m_eCurDir == DIR_RIGHT)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Skill3");

		m_tFrame = { 0, (_float)m_vecTexture.size() };
		CObj* pObj = CAbstractFactory<CIceOrb>::CreateObj(*m_pTransform->Get_StateInfo(STATE_POSITION), vec3(1.f, 1.f, 1.f), (int)m_eCurDir);
		if (nullptr == pObj)
			return 0;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_BULLET);

		vec3 vPos = *m_pTransform->Get_StateInfo(STATE_POSITION);
		vPos.y += 0.5f;
		pObj = CAbstractFactory<CCast>::CreateObj(vPos, vec3(1.f, 1.f, 1.f), (int)m_eCurDir);
		if (nullptr == pObj)
			return 0;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_EFFECT);
	}
	else if (CKeyMgr::GetInstance()->KeyDown(KEY_X))
	{

		CSoundMgr::GetInstance()->PlayOnce(L"C_IceBolt.wav", CSoundMgr::SOUND_PLAYER);
		m_ePlayerState = SKILL;
		if (m_eCurDir == DIR_UP)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Skill0");
		else if (m_eCurDir == DIR_DOWN)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Skill8");
		else if (m_eCurDir == DIR_LEFT)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Skill11");
		else if (m_eCurDir == DIR_RIGHT)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Skill3");

		m_tFrame = {0, (_float)m_vecTexture.size() };
		CObj* pObj = CAbstractFactory<CIceBolt>::CreateObj(*m_pTransform->Get_StateInfo(STATE_POSITION), vec3(1.f, 1.f, 1.f), (int)m_eCurDir);
		if (nullptr == pObj)
			return 0;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_BULLET);

		vec3 vPos = *m_pTransform->Get_StateInfo(STATE_POSITION);
		vPos.y += 0.2f;
		pObj = CAbstractFactory<CCast>::CreateObj(vPos, vec3(1.f, 1.f, 1.f), (int)m_eCurDir);
		if (nullptr == pObj)
			return 0;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_EFFECT);
	}
	else if (CKeyMgr::GetInstance()->KeyDown(KEY_C))
	{
		CSoundMgr::GetInstance()->PlayOnce(L"C_FrozenArmor.wav", CSoundMgr::SOUND_PLAYER);
		m_ePlayerState = SKILL;
		m_tFrame = { 0, (_float)m_vecTexture.size() };
		CObj* pObj = CAbstractFactory<CArmor>::CreateObj(*m_pTransform->Get_StateInfo(STATE_POSITION), vec3(1.f, 1.f, 1.f), (int)m_eCurDir);
		if (nullptr == pObj)
			return 0;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_EFFECT);

			vec3 vPos = *m_pTransform->Get_StateInfo(STATE_POSITION);
		vPos.y += 0.5f;
		pObj = CAbstractFactory<CCast>::CreateObj(vPos, vec3(1.f, 1.f, 1.f), (int)m_eCurDir);
		if (nullptr == pObj)
			return 0;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_EFFECT);
	}

	return 0;
}

void CPlayer::Late_Update(const _float& fTimeDelta)
{
	if (m_IsHit)
	{
		CSoundMgr::GetInstance()->PlayOnce(L"P_Hit.wav", CSoundMgr::SOUND_PLAYER);
		m_tInfo.iHp -= 1;
		m_ePlayerState = DAMAGE;

		if (m_eCurDir == DIR_UP)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Damage0");
		else if (m_eCurDir == DIR_DOWN)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Damage8");
		else if (m_eCurDir == DIR_LEFT)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Damage11");
		else if (m_eCurDir == DIR_RIGHT)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Player_Damage3");

		m_tFrame = {0, (_float)m_vecTexture.size()};
		m_IsHit = false;
	}
}

void CPlayer::Render()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glUseProgram(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum());

	mat4 matView = CCameraMgr::GetInstance()->Get_ViewMatrix();
	mat4 matProj = CCameraMgr::GetInstance()->Get_ProjMatrix();
	_uint WorldLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matWorld");
	glUniformMatrix4fv(WorldLocation, 1, GL_FALSE, value_ptr(m_pTransform->GetMatrix()));

	_uint ViewLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matView");
	glUniformMatrix4fv(ViewLocation, 1, GL_FALSE, &matView[0][0]);

	_uint ProjLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matProj");
	glUniformMatrix4fv(ProjLocation, 1, GL_FALSE, &matProj[0][0]);

	_uint TextureLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "outTexture");
	glUniform1i(TextureLocation, 0);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_vecTexture[m_tFrame.fFrame]);
	m_iCurTextureIdx++;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	m_pBuffer->Render();
	glDisable(GL_BLEND);


	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void CPlayer::Release()
{
	SafeDelete(m_pBuffer);
	SafeDelete(m_pTransform);
}
