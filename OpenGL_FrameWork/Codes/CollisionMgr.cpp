#include "pch.h"
#include "..\Headers\CollisionMgr.h"
#include "Transform.h"
#include "Obj.h"
_IMPLEMENT_SINGLETON(CCollisionMgr)
CCollisionMgr::CCollisionMgr()
{
}


CCollisionMgr::~CCollisionMgr()
{
}

_bool CCollisionMgr::Check_Collision(vector<CObj*> pSourLst, vector<CObj*> pDestLst, float fTemp)
{
	for (auto& iter_Sour : pSourLst)
	{
		for (auto& iter_Dest : pDestLst)
		{
 			mat4 matSour = iter_Sour->GetTransform()->GetMatrix();
			mat4 matDest = iter_Dest->GetTransform()->GetMatrix();

			_float fSour_xMin = iter_Sour->GetTransform()->Get_StateInfo(STATE_POSITION)->x - iter_Sour->GetTransform()->GetScale().x/fTemp;
			_float fSour_xMax = iter_Sour->GetTransform()->Get_StateInfo(STATE_POSITION)->x + iter_Sour->GetTransform()->GetScale().x/fTemp;
			
			_float fDest_xMin = iter_Dest->GetTransform()->Get_StateInfo(STATE_POSITION)->x - iter_Sour->GetTransform()->GetScale().x/fTemp;
			_float fDest_xMax = iter_Dest->GetTransform()->Get_StateInfo(STATE_POSITION)->x + iter_Sour->GetTransform()->GetScale().x/fTemp;
			
			_float fSour_yMin = iter_Sour->GetTransform()->Get_StateInfo(STATE_POSITION)->y- iter_Sour->GetTransform()->GetScale().y/fTemp;
			_float fSour_yMax = iter_Sour->GetTransform()->Get_StateInfo(STATE_POSITION)->y+ iter_Sour->GetTransform()->GetScale().y/fTemp;
			
			_float fDest_yMin = iter_Dest->GetTransform()->Get_StateInfo(STATE_POSITION)->y - iter_Sour->GetTransform()->GetScale().y/fTemp;
			_float fDest_yMax = iter_Dest->GetTransform()->Get_StateInfo(STATE_POSITION)->y + iter_Sour->GetTransform()->GetScale().y/fTemp;


			if (fSour_xMax < fDest_xMin || fSour_xMin>fDest_xMax)
				continue;
			if (fSour_yMax < fDest_yMin || fSour_yMin>fDest_yMax)
				continue;
			else
			{
 				iter_Sour->GetIsHit() = true;	
				iter_Dest->GetIsHit() = true;
				iter_Sour->GetIsDead() = true;
 				return true;
			}


		}
	}

	return false;
}