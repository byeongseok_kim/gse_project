#include "pch.h"
#include "Demon.h"
#include "Buffer_RectTex.h"
#include "Shader.h"
#include "Transform.h"
#include "Texture.h"
#include "Player.h"
#include "DemonSkill.h"

CDemon::CDemon()
{
}

CDemon::~CDemon()
{
	Release();
}

HRESULT CDemon::Init()
{
	return S_OK;
}

HRESULT CDemon::Init(vec3 vPos, vec3 vSize, _int iIdx)
{
	m_pBuffer = new CBuffer_RectTex;
	m_pTransform = new CTransform;
	m_pBuffer->Init();
	m_pTransform->SetUp_Speed(0.3f, radians(5.f));

	m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Idle8");

	m_tFrame = { 0, (_float)m_vecTexture.size() };
	m_eState = IDLE;
	m_eCurDir = DIR_DOWN;
	m_eID = ID_GAMEOBJECT;
	m_tInfo.iHp = 10;
	return S_OK;
}

int CDemon::Update(const _float& fTimeDelta)
{
	if (m_tInfo.iHp <= 0)
	{		CSoundMgr::GetInstance()->PlayOnce(L"M_Ddeath.wav", CSoundMgr::SOUND_MONSTER);
		CObj::MoveFrame(fTimeDelta / 1000.f, m_IsDead);

		if (m_eCurDir == DIR_DOWN)
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Death8");
		}
		else if (m_eCurDir == DIR_UP)
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Death0");
		}
		else if (m_eCurDir == DIR_LEFT)
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Death12");
		}
		else if (m_eCurDir == DIR_RIGHT)
		{
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Death4");
		}
		if (m_tFrame.fFrame > m_tFrame.fMax)
			m_tFrame.fFrame = 0.f;
		m_tFrame.fMax = (_float)m_vecTexture.size();
		m_eState = DEATH;
	}
	else
	{
		if (m_eState == IDLE)
			CObj::MoveFrame(fTimeDelta / 1000.f);
		else
		{
			if (!CObj::MoveFrame_Once(fTimeDelta / 1000.f))
			{
				if (m_eCurDir == DIR_DOWN)
				{
					m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Idle8");
				}
				else if (m_eCurDir == DIR_UP)
				{
					m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Idle0");
				}
				else if (m_eCurDir == DIR_LEFT)
				{
					m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Idle12");
				}
				else if (m_eCurDir == DIR_RIGHT)
				{
					m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Idle4");
				}
				m_tFrame = { 0, (_float)m_vecTexture.size() };
				m_eState = IDLE;
			}
		}
		Pattern(fTimeDelta);
	}

	




	

	if (m_IsDead)
		return 1;
	return 0;
}

void CDemon::Late_Update(const _float& fTimeDelta)
{
	if (m_IsHit)
	{
		CSoundMgr::GetInstance()->PlayOnce(L"F_Hit0.wav", CSoundMgr::SOUND_MONSTER);
		m_tInfo.iHp -= 1;
		m_eState = DAMAGE;

		if (m_eCurDir == DIR_UP)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Damaged0");
		else if (m_eCurDir == DIR_DOWN)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Damaged8");
		else if (m_eCurDir == DIR_LEFT)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Damaged12");
		else if (m_eCurDir == DIR_RIGHT)
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Damaged4");

		m_tFrame = { 0, (_float)m_vecTexture.size() };
		m_IsHit = false;
	}
}

void CDemon::Render()
{
	glUseProgram(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum());

	mat4 matView = CCameraMgr::GetInstance()->Get_ViewMatrix();
	mat4 matProj = CCameraMgr::GetInstance()->Get_ProjMatrix();
	_uint WorldLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matWorld");
	glUniformMatrix4fv(WorldLocation, 1, GL_FALSE, value_ptr(m_pTransform->GetMatrix()));

	_uint ViewLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matView");
	glUniformMatrix4fv(ViewLocation, 1, GL_FALSE, &matView[0][0]);

	_uint ProjLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matProj");
	glUniformMatrix4fv(ProjLocation, 1, GL_FALSE, &matProj[0][0]);

	_uint TextureLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "outTexture");
	glUniform1i(TextureLocation, 0);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_vecTexture[m_tFrame.fFrame]);


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	m_pBuffer->Render();


	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void CDemon::Release()
{
	SafeDelete(m_pBuffer);
	SafeDelete(m_pTransform);
}

void CDemon::RandomDir()
{
	_uint iRand = rand() % 4;
	m_eCurDir = (DIR)iRand;
}

void CDemon::Movement(const _float& fTimeDelta)
{
	m_eState = IDLE;
	if (m_eCurDir == DIR_DOWN)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Dash8");
		m_pTransform->Go_Down(fTimeDelta);
	}
	else if (m_eCurDir == DIR_UP)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Dash0");
		m_pTransform->Go_Up(fTimeDelta);
	}
	else if (m_eCurDir == DIR_LEFT)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Dash12");
		m_pTransform->Go_Left(fTimeDelta);
	}
	else if (m_eCurDir == DIR_RIGHT)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Dash4");
		m_pTransform->Go_Right(fTimeDelta);
	}
}

void CDemon::Movement(const vec3& vPlayerPos)
{
	m_eState = IDLE;
	vec3 vPos = *m_pTransform->Get_StateInfo(STATE_POSITION);
	_float fLenghtX = 0.f;
	_float fLenghtY = 0.f;

	fLenghtX = fabs(length(vPlayerPos.x - vPos.x));
	fLenghtY = fabs(length(vPlayerPos.y - vPos.y));

	if (fLenghtX > fLenghtY)
	{
		if (vPlayerPos.x > vPos.x)
		{
			m_eCurDir = DIR_RIGHT;
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Dash4");
		}
		else
		{
			m_eCurDir = DIR_LEFT;
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Dash12");
		}
	}
	else
	{
		if (vPlayerPos.y > vPos.y)
		{
			m_eCurDir = DIR_UP;
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Dash0");
		}
		else
		{
			m_eCurDir = DIR_DOWN;
			m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Dash8");
		}
	}

	if (m_vecTexture.size() < m_tFrame.fFrame)
		m_tFrame.fFrame = 0;
	m_tFrame.fMax = (_float)m_vecTexture.size();
}

void CDemon::Pattern(const _float& fTimeDelta)
{
	m_fSkillTime += fTimeDelta / 1000.f;
	_float fLength = 0.f;
	CObj* pPlayer = CObjectMgr::GetInstance()->GetObject_List(OBJECT_PLAYER)[0];
	vec3 vPlayerPos = *pPlayer->GetTransform()->Get_StateInfo(STATE_POSITION);
	fLength = length(vPlayerPos - *m_pTransform->Get_StateInfo(STATE_POSITION));

	if (fLength <= 1.f)
	{
		//Attack or Skill
		if (m_fSkillTime > 2.f)
		{
			Attack();
			m_fSkillTime = 0.f;
		}
	}
	else if (fLength <= 2.f)
	{
		m_eState = IDLE;
		// Follow Player
		Movement(vPlayerPos);
		m_pTransform->Go_ToTarget(&vPlayerPos, fTimeDelta / 1000.f);
	}
	else
	{
		m_fSelectDirTime += fTimeDelta / 1000.f;
		if (m_fSelectDirTime > 2.f)
		{
			RandomDir();
			m_fSelectDirTime = 0.f;
		}
		Movement(fTimeDelta / 1000.f);
	}
}

void CDemon::Attack()
{
	CSoundMgr::GetInstance()->PlayOnce(L"M_Dattack.wav", CSoundMgr::SOUND_MONSTER);
	m_eState = SKILL;
	if (m_eCurDir == DIR_DOWN)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Attack8");
	}
	else if (m_eCurDir == DIR_UP)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Attack0");
	}
	else if (m_eCurDir == DIR_LEFT)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Attack12");
	}
	else if (m_eCurDir == DIR_RIGHT)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Demon_Attack4");
	}
	m_tFrame = {0, (_float)m_vecTexture.size()};
	CObj* pObj = CAbstractFactory<CDemonSkill>::CreateObj(*m_pTransform->Get_StateInfo(STATE_POSITION), vec3(1.f, 1.f, 1.f), (int)m_eCurDir);
	if (nullptr == pObj)
		return;
	CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_MONSTER_BULLET);
}


