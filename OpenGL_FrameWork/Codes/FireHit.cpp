#include "pch.h"
#include "FireHit.h"
#include "Buffer_RectTex.h"
#include "Shader.h"
#include "Transform.h"
#include "Texture.h"
CFireHit::CFireHit()
{
}

CFireHit::~CFireHit()
{
	Release();
}

HRESULT CFireHit::Init()
{
	return S_OK;
}

HRESULT CFireHit::Init(vec3 vPos, vec3 vSize, _int iIdx)
{
	m_pBuffer = new CBuffer_RectTex;
	m_pTransform = new CTransform;
	m_pBuffer->Init();
	m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	m_pTransform->SetUp_Speed(1.f, radians(5.f));
	m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Fire_Hit");
	m_tFrame = { 0, (_float)m_vecTexture.size() };
	m_eCurDir = (DIR)iIdx;
	m_eID = ID_EFFECT;
	return S_OK;
}

int CFireHit::Update(const _float& fTimeDelta)
{
	CObj::MoveFrame(fTimeDelta / 1000.f, m_IsDead);

	if (m_IsDead)
		return 1;
	return 0;
}

void CFireHit::Late_Update(const _float& fTimeDelta)
{
}

void CFireHit::Render()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glUseProgram(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum());

	mat4 matView = CCameraMgr::GetInstance()->Get_ViewMatrix();
	mat4 matProj = CCameraMgr::GetInstance()->Get_ProjMatrix();
	_uint WorldLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matWorld");
	glUniformMatrix4fv(WorldLocation, 1, GL_FALSE, value_ptr(m_pTransform->GetMatrix()));

	_uint ViewLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matView");
	glUniformMatrix4fv(ViewLocation, 1, GL_FALSE, &matView[0][0]);

	_uint ProjLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matProj");
	glUniformMatrix4fv(ProjLocation, 1, GL_FALSE, &matProj[0][0]);

	_uint TextureLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "outTexture");
	glUniform1i(TextureLocation, 0);



	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_vecTexture[m_tFrame.fFrame]);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	m_pBuffer->Render();
	glDisable(GL_BLEND);


	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void CFireHit::Release()
{
	SafeDelete(m_pBuffer);
	SafeDelete(m_pTransform);
}
