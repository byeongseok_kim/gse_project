#include "pch.h"
#include "..\Headers\Obj.h"
#include "Texture.h"
#include "Transform.h"

CObj::CObj()
{
}


CObj::~CObj()
{
}

HRESULT CObj::Init(vec3 vPos, vec3 vSize, vec3 vColor)
{
	return E_NOTIMPL;
}

HRESULT CObj::Init(vec3 vPos, vec3 vSize, vec3 vColor, CObj* pTarget)
{
	return E_NOTIMPL;
}

HRESULT CObj::Init(vec3 vLightPos, vec3 vLightColor, string strLightName)
{
	return E_NOTIMPL;
}

HRESULT CObj::Init(vec3 vP1, vec3 vP2, vec3 vP3, bool IsParabolic)
{
	return E_NOTIMPL;
}

HRESULT CObj::Init(vec3 vPos, vec3 vSize, _int iIdx)
{
	return E_NOTIMPL;
}

void CObj::Late_Update(const _float& fTimeDelta)
{
}

void CObj::MoveFrame(const _float& fTimeDelta)
{
	m_tFrame.fFrame += m_tFrame.fMax * fTimeDelta;

	if (m_tFrame.fMax < m_tFrame.fFrame)
		m_tFrame.fFrame = 0.f;
}

void CObj::MoveFrame(const _float& fTimeDelta, _bool& IsDead)
{
	m_tFrame.fFrame += m_tFrame.fMax * fTimeDelta;

	if (m_tFrame.fMax < m_tFrame.fFrame)
		IsDead = true;
}

bool CObj::MoveFrame_Once(const _float& fTimeDelta)
{
	m_tFrame.fFrame += m_tFrame.fMax * fTimeDelta;

	if (m_tFrame.fMax < m_tFrame.fFrame)
		return false;
	return true;
}


void CObj::Compute_RUL(vec3 vPos)
{
	vec3 vTempPos = *m_pTransform->Get_StateInfo(STATE_POSITION);
	vTempPos.y = 1.f;

	vec3 vLook = vPos - vTempPos;
	vLook = normalize(vLook);

	vec3 vUp = vec3(0.f, 1.f, 0.f);
	vec3 vRight = cross(vUp, vLook);

	vLook *= -1;
	m_pTransform->Set_StateInfo(STATE_LOOK, &vLook);
	m_pTransform->Set_StateInfo(STATE_RIGHT, &vRight);
	m_pTransform->Set_StateInfo(STATE_UP, &vUp);
}

void CObj::Compute_RULO(vec3 vPos)
{
	vec3 vTempPos = *m_pTransform->Get_StateInfo(STATE_POSITION);
	vTempPos.y = 1.f;

	vPos.y = 1.f;
	vec3 vLook = vPos - vTempPos;
	vLook = normalize(vLook);

	vec3 vUp = vec3(0.f, 1.f, 0.f);
	vec3 vRight = cross(vUp, vLook);


	vLook *= -1;
	m_pTransform->Set_StateInfo(STATE_LOOK, &vLook);
	m_pTransform->Set_StateInfo(STATE_RIGHT, &vRight);
	m_pTransform->Set_StateInfo(STATE_UP, &vUp);
}

