#include "pch.h"
#include "..\Headers\Camera_Debug.h"
#include "Shader.h"
#include "Transform.h"
#include "Player.h"

CCamera_Debug::CCamera_Debug()
{
}


CCamera_Debug::~CCamera_Debug()
{
}

HRESULT CCamera_Debug::Init()
{
	m_matView = mat4(1.f);
 	m_matProj = mat4(1.f);
	 
	ZeroMemory(&m_tProjDesc, sizeof(PROJDESC));
	ZeroMemory(&m_tCameraDesc, sizeof(CAMERADESC));

	m_pTransform = new CTransform;

	m_pTransform->SetUp_Speed(0.01f, radians(5.f));

	return S_OK;
}

int CCamera_Debug::Update(const _float& fTimeDelta)
{
	///SetCursorPos(400, 350);
	glutSetCursor(GLUT_CURSOR_NONE);

	float fX=CObjectMgr::GetInstance()->GetObject_List(OBJECT_PLAYER)[0]->GetTransform()->Get_StateInfo(STATE_POSITION)->x;
	float fY=CObjectMgr::GetInstance()->GetObject_List(OBJECT_PLAYER)[0]->GetTransform()->Get_StateInfo(STATE_POSITION)->y;
	float fZ = m_pTransform->Get_StateInfo(STATE_POSITION)->z;
	vec3 vPos = vec3(fX, fY, fZ);
	m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);


	Invalidate_ViewProjMatrix();
	return 0;
}
void CCamera_Debug::Late_Update(const _float& fTimeDelta)
{
}


void CCamera_Debug::Render()
{
}

void CCamera_Debug::Release()
{
	SafeDelete(m_pTransform);
}
