#include "pch.h"
#include "..\Headers\CameraMgr.h"
#include "Camera_Debug.h"

#include "Transform.h"

_IMPLEMENT_SINGLETON(CCameraMgr)
CCameraMgr::CCameraMgr()
{
}


CCameraMgr::~CCameraMgr()
{
}

void CCameraMgr::Swap_Camera(CAMERA eCameraMode)
{
	m_eCameraMode = eCameraMode;
	if (m_eCameraMode != m_ePreCameraMode)
	{
		CObjectMgr::GetInstance()->Group_Release(OBJECT_CAMERA);
		switch (eCameraMode)
		{
			
		case CAMERA_DEBUG:
			SetUp_CameraOption(eCameraMode);
			break;
		case CAMERA_TOPVIEW:
			SetUp_CameraOption(eCameraMode);
			break;
		case CAMERA_PLAYER:
			SetUp_CameraOption(eCameraMode);
			break;
		}
		m_ePreCameraMode = eCameraMode;
	}

}

HRESULT CCameraMgr::SetUp_CameraOption(CAMERA eCameraMode)
{
	CAMERADESC		tCameraDesc;
	ZeroMemory(&tCameraDesc, sizeof(CAMERADESC));
	tCameraDesc.vEye = vec3(0.f, 0.f, 3.f);
	tCameraDesc.vAt = vec3(0.f, 0.f, 0.f);
	tCameraDesc.vAxisY = vec3(0.f, 1.f, 0.f);

	PROJDESC		tProjDesc;
	ZeroMemory(&tProjDesc, sizeof(tProjDesc));
	tProjDesc.fFovY = radians(60.f);
	tProjDesc.fAspect = _float(WINCX) / WINCY;
	tProjDesc.fNear = 0.2f;
	tProjDesc.fFar = 2000.f;

	if (eCameraMode == CAMERA_DEBUG)
	{
		CObj* pObj = CAbstractFactory<CCamera_Debug>::CreateObj();
		if (nullptr == pObj)
			return E_FAIL;
		dynamic_cast<CCamera_Debug*>(pObj)->SetUp_CameraProjDesc(tCameraDesc, tProjDesc);
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_CAMERA);
	}

	eCamera_Mode = eCameraMode;

	return S_OK;

}
