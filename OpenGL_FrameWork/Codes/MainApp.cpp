#include "pch.h"
#include "MainApp.h"
#include "Shader.h"

#include "Rect.h"
#include "Player.h"
#include "Demon.h"
#include "Cow.h"
#include "Terrain.h"
#include "Diablo.h"

CMainApp::CMainApp()
{
}


CMainApp::~CMainApp()
{
	Release();
}

HRESULT CMainApp::Init()
{
	HWND hWnd = GetActiveWindow();
	HINSTANCE hInstC = GetModuleHandle(0);

	if (FAILED(CShaderMgr::GetInstance()->AddShader(L"Shader_Default", "../ShaderFiles/VertexShader.txt", "../ShaderFiles/FragmentShader.txt")))
		return E_FAIL;
	if (FAILED(CShaderMgr::GetInstance()->AddShader(L"Shader_Texture", "../ShaderFiles/Vertex_Shader_Texture.txt", "../ShaderFiles/Fragment_Shader_Texture.txt")))
		return E_FAIL;
	if (FAILED(CShaderMgr::GetInstance()->AddShader(L"Shader_CubeTexture", "../ShaderFiles/Vertex_Shader_Texture_Cube.txt", "../ShaderFiles/Fragment_Shader_Texture_Cube.txt")))
		return E_FAIL;
	if (FAILED(CShaderMgr::GetInstance()->AddShader(L"Shader_Terrain", "../ShaderFiles/Vertex_Shader_Terrain.txt", "../ShaderFiles/Fragment_Shader_Terrain.txt")))
		return E_FAIL;
	if (FAILED(CInputMgr::GetInstance()->Ready_Input_Device(hInstC, hWnd)))
		return E_FAIL;


	if (FAILED(Create_Player_Texture()))
		return E_FAIL;
	if (FAILED(Create_Effect_Texture()))
		return E_FAIL;
	if (FAILED(Create_Demon_Texture()))
		return E_FAIL;
	if (FAILED(Create_Demon_Skill_Texture()))
		return E_FAIL;
	if (FAILED(Create_Cow_Texture()))
		return E_FAIL;
	if (FAILED(Create_Attack_Range_Texture()))
		return E_FAIL;
	if (FAILED(CReate_Map_Texture()))
		return E_FAIL;
	if (FAILED(Create_Diablo_Texture()))
		return E_FAIL;
	CSoundMgr::GetInstance()->Initialize();
	
	if (CCameraMgr::GetInstance()->SetUp_CameraOption(CAMERA_DEBUG))
		return E_FAIL;



	CObj* pObj = CAbstractFactory<CPlayer>::CreateObj();
	if (nullptr == pObj)
		return E_FAIL;
	CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_PLAYER);

	if (FAILED(Random_Create_Monster()))
		return E_FAIL;

	pObj = CAbstractFactory<CTerrain>::CreateObj();
	if (nullptr == pObj)
		return E_FAIL;
	CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_TERRAIN);
	return S_OK;
}

void CMainApp::Update(const _float& fTimeDelta)
{
	srand(unsigned(time(NULL)));
	CSoundMgr::GetInstance()->Update();
	CInputMgr::GetInstance()->SetUp_InputState();
	CKeyMgr::GetInstance()->Update();


	CObjectMgr::GetInstance()->Update(fTimeDelta);
	CObjectMgr::GetInstance()->LateUpdate(fTimeDelta);

	if (g_IsEnd)
	{
		char szInput;
		cout << "Game End" << endl;
		cout << "Please Press ESC ";
	}
}

void CMainApp::Render()
{
	CObjectMgr::GetInstance()->Render();
}

void CMainApp::Release()
{
	CObjectMgr::GetInstance()->DestroyInstance();
	CKeyMgr::GetInstance()->DestroyInstance();
	CShaderMgr::GetInstance()->DestroyInstance();
	CCameraMgr::GetInstance()->DestroyInstance();
	CInputMgr::GetInstance()->DestroyInstance();
	CTextureMgr::GetInstance()->DestroyInstance();
	CCollisionMgr::GetInstance()->DestroyInstance();
	CSoundMgr::GetInstance()->DestroyInstance();
}

HRESULT CMainApp::Create_Player_Texture()
{
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Idle0", "../Bin/Resource/Player/stand_0/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Idle3", "../Bin/Resource/Player/stand_3/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Idle8", "../Bin/Resource/Player/stand_8/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Idle11", "../Bin/Resource/Player/stand_11/%d.png", 8)))
		return E_FAIL;


	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Walk0", "../Bin/Resource/Player/walk_0/%d.png", 8)))
		return E_FAIL;																									
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Walk3", "../Bin/Resource/Player/walk_3/%d.png", 8)))
		return E_FAIL;																												
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Walk8", "../Bin/Resource/Player/walk_8/%d.png", 8)))
		return E_FAIL;																									  
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Walk11", "../Bin/Resource/Player/walk_11/%d.png", 8)))
		return E_FAIL;																							  

	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Attack0", "../Bin/Resource/Player/attack_0/%d.png", 18)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Attack3", "../Bin/Resource/Player/attack_3/%d.png", 18)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Attack8", "../Bin/Resource/Player/attack_8/%d.png", 18)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Attack11", "../Bin/Resource/Player/attack_11/%d.png", 18)))
		return E_FAIL;

	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Skill0", "../Bin/Resource/Player/skill_0/%d.png", 14)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Skill3", "../Bin/Resource/Player/skill_3/%d.png", 14)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Skill8", "../Bin/Resource/Player/skill_8/%d.png", 14)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Skill11", "../Bin/Resource/Player/skill_11/%d.png", 14)))
		return E_FAIL;

	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Damage0", "../Bin/Resource/Player/damage_0/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Damage3", "../Bin/Resource/Player/damage_3/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Damage8", "../Bin/Resource/Player/damage_8/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Player_Damage11", "../Bin/Resource/Player/damage_11/%d.png", 8)))
		return E_FAIL;

	return S_OK;
}

HRESULT CMainApp::Create_Effect_Texture()
{
	//Ice Bolt
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Ice_Bolt0", "../Bin/Resource/Effect/Cold/IceBolt/0/%d.png", 6)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Ice_Bolt4", "../Bin/Resource/Effect/Cold/IceBolt/4/%d.png", 6)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Ice_Bolt8", "../Bin/Resource/Effect/Cold/IceBolt/8/%d.png", 6)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Ice_Bolt12", "../Bin/Resource/Effect/Cold/IceBolt/12/%d.png", 6)))
		return E_FAIL;

	// Ice Hit
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Hit0", "../Bin/Resource/Effect/Cold/Hit/0/%d.png", 16)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Hit1", "../Bin/Resource/Effect/Cold/Hit/1/%d.png", 25)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Hit2", "../Bin/Resource/Effect/Cold/Hit/2/%d.png",15)))
		return E_FAIL;

	//Cast
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cast", "../Bin/Resource/Effect/Cold/Cast/%d.png", 15)))
		return E_FAIL;

	//Armor
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Armor", "../Bin/Resource/Effect/Cold/FrozenArmor/%d.png", 24)))
		return E_FAIL;
	//IceOrb
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"IceOrb", "../Bin/Resource/Effect/Cold/FrozenOrb/%d.png", 16)))
		return E_FAIL;


	return S_OK;
}

HRESULT CMainApp::Create_Demon_Texture()
{
	//Demon Att
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Attack0", "../Bin/Resource/Demon/attack_0/%d.png", 16)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Attack4", "../Bin/Resource/Demon/attack_4/%d.png", 16)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Attack8", "../Bin/Resource/Demon/attack_8/%d.png", 16)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Attack12", "../Bin/Resource/Demon/attack_12/%d.png", 16)))
		return E_FAIL;

	//Demon Damage
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Damaged0", "../Bin/Resource/Demon/damage_0/%d.png", 6)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Damaged4", "../Bin/Resource/Demon/damage_4/%d.png", 6)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Damaged8", "../Bin/Resource/Demon/damage_8/%d.png", 6)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Damaged12", "../Bin/Resource/Demon/damage_12/%d.png", 6)))
		return E_FAIL;

	//Demon Dash
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Dash0", "../Bin/Resource/Demon/dash_0/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Dash4", "../Bin/Resource/Demon/dash_4/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Dash8", "../Bin/Resource/Demon/dash_8/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Dash12", "../Bin/Resource/Demon/dash_12/%d.png", 8)))
		return E_FAIL;

	//Demon Idle
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Idle0", "../Bin/Resource/Demon/stand_0/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Idle4", "../Bin/Resource/Demon/stand_4/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Idle8", "../Bin/Resource/Demon/stand_8/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Idle12", "../Bin/Resource/Demon/stand_12/%d.png", 8)))
		return E_FAIL;

	//Demon Death
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Death0", "../Bin/Resource/Demon/death_0/%d.png", 20)))
		return E_FAIL;         
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Death4", "../Bin/Resource/Demon/death_4/%d.png", 20)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Death8", "../Bin/Resource/Demon/death_8/%d.png", 20)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Demon_Death12", "../Bin/Resource/Demon/death_12/%d.png", 20)))
		return E_FAIL;


	return S_OK;
}         
HRESULT CMainApp::Create_Diablo_Texture()
{
	//Diablo Damage
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Damaged0", "../Bin/Resource/Diablo/GetHit_0/%d.png", 6)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Damaged4", "../Bin/Resource/Diablo/GetHit_4/%d.png", 6)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Damaged8", "../Bin/Resource/Diablo/GetHit_8/%d.png", 6)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Damaged12", "../Bin/Resource/Diablo/GetHit_12/%d.png", 6)))
		return E_FAIL;

	//Diablo Dash
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Dash0", "../Bin/Resource/Diablo/Run_0/%d.png", 22)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Dash4", "../Bin/Resource/Diablo/Run_4/%d.png", 22)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Dash8", "../Bin/Resource/Diablo/Run_8/%d.png", 22)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Dash12", "../Bin/Resource/Diablo/Run_12/%d.png", 22)))
		return E_FAIL;

	//Diablo Idle
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Idle0", "../Bin/Resource/Diablo/Netural_0/%d.png", 12)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Idle4", "../Bin/Resource/Diablo/Netural_4/%d.png",12)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Idle8", "../Bin/Resource/Diablo/Netural_8/%d.png", 12)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Idle12", "../Bin/Resource/Diablo/Netural_12/%d.png", 12)))
		return E_FAIL;

	//Diablo Death
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Death0", "../Bin/Resource/Diablo/death_0/%d.png", 142)))
		return E_FAIL;

	//Diablo Skill1
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Skill0_0", "../Bin/Resource/Diablo/FireCast_0/%d.png", 18)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Skill0_4", "../Bin/Resource/Diablo/FireCast_4/%d.png", 18)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Skill0_8", "../Bin/Resource/Diablo/FireCast_8/%d.png", 18)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Skill0_12", "../Bin/Resource/Diablo/FireCast_12/%d.png", 18)))
		return E_FAIL;

	//Diablo Skill1
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Skill1_0", "../Bin/Resource/Diablo/SpecialCast_0/%d.png", 16)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Skill1_4", "../Bin/Resource/Diablo/SpecialCast_4/%d.png", 16)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Skill1_8", "../Bin/Resource/Diablo/SpecialCast_8/%d.png", 16)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Diablo_Skill1_12", "../Bin/Resource/Diablo/SpecialCast_12/%d.png", 16)))
		return E_FAIL;

	return S_OK;
}
HRESULT CMainApp::Create_Cow_Texture()
{
	// Cow IDLE
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Idle0", "../Bin/Resource/Cow/stand_0/%d.png", 10)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Idle4", "../Bin/Resource/Cow/stand_4/%d.png", 10)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Idle8", "../Bin/Resource/Cow/stand_8/%d.png", 10)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Idle12", "../Bin/Resource/Cow/stand_12/%d.png", 10)))
		return E_FAIL;

	// Cow Attack
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Attack0", "../Bin/Resource/Cow/attack_0/%d.png", 19)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Attack4", "../Bin/Resource/Cow/attack_4/%d.png", 19)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Attack8", "../Bin/Resource/Cow/attack_8/%d.png", 19)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Attack12", "../Bin/Resource/Cow/attack_12/%d.png", 19)))
		return E_FAIL;

	// Cow Walk
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Walk0", "../Bin/Resource/Cow/dash_0/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Walk4", "../Bin/Resource/Cow/dash_4/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Walk8", "../Bin/Resource/Cow/dash_8/%d.png", 8)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Walk12", "../Bin/Resource/Cow/dash_12/%d.png", 8)))
		return E_FAIL;
	
	// Cow Damage
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Damage0", "../Bin/Resource/Cow/damage_0/%d.png", 5)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Damage4", "../Bin/Resource/Cow/damage_4/%d.png", 5)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Damage8", "../Bin/Resource/Cow/damage_8/%d.png", 5)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Damage12", "../Bin/Resource/Cow/damage_12/%d.png", 5)))
		return E_FAIL;

	//Cow Death
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Death0", "../Bin/Resource/Cow/death_0/%d.png", 14)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Death4", "../Bin/Resource/Cow/death_4/%d.png", 14)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Death8", "../Bin/Resource/Cow/death_8/%d.png", 14)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Cow_Death12", "../Bin/Resource/Cow/death_12/%d.png", 14)))
		return E_FAIL;
	return S_OK;
}
HRESULT CMainApp::Create_Demon_Skill_Texture()
{                     
	//Fire Hit
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Fire_Hit", "../Bin/Resource/Effect/Fire/Hit/%d.png", 12)))
		return E_FAIL;

	//Fire Bolt
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"FireBolt0", "../Bin/Resource/Effect/Fire/FireBolt/0/%d.png", 5)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"FireBolt4", "../Bin/Resource/Effect/Fire/FireBolt/4/%d.png", 5)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"FireBolt8", "../Bin/Resource/Effect/Fire/FireBolt/8/%d.png", 5)))
		return E_FAIL;
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"FireBolt12", "../Bin/Resource/Effect/Fire/FireBolt/12/%d.png", 5)))
		return E_FAIL;

	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Inferno", "../Bin/Resource/Effect/Fire/Inferno/%d.png", 15)))
		return E_FAIL;

	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Fire", "../Bin/Resource/Effect/Fire/Fire/big/%d.png", 76)))
		return E_FAIL;
	return S_OK;
}
HRESULT CMainApp::Create_Attack_Range_Texture()
{
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Attack_Range", "../Bin/Resource/Red%d.png", 1)))
		return E_FAIL;
	return S_OK;
}
HRESULT CMainApp::CReate_Map_Texture()
{
	if (FAILED(CTextureMgr::GetInstance()->Add_Texture(L"Map", "../Bin/Resource/Map%d.png", 1)))
		return E_FAIL;
	return S_OK;
}

HRESULT CMainApp::Random_Create_Monster()
{
	int iRand_Cow = 17;
	int iRand_Demon = 2;
	int iRand_Diablo = 1;

	for (int i = 0; i < iRand_Cow; ++i)
	{
		CObj* pObj = CAbstractFactory<CCow>::CreateObj(vec3(rand()%100, rand() % 100, 0.f), vec3(1.f, 1.f, 1.f), 0);
		if (nullptr == pObj)
			return E_FAIL;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_MONSTER);
	}

	for (int i = 0; i < iRand_Demon; ++i)
	{
		CObj* pObj = CAbstractFactory<CDemon>::CreateObj(vec3(rand() % 100, rand() % 100, 0.f), vec3(1.f, 1.f, 1.f), 0);
		if (nullptr == pObj)
			return E_FAIL;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_MONSTER);
	}

	for (int i = 0; i < iRand_Diablo; ++i)
	{
		CObj* pObj = CAbstractFactory<CDiablo>::CreateObj(vec3(rand() % 100, rand() % 100, 0.f), vec3(1.f, 1.f, 1.f), 0);
		if (nullptr == pObj)
			return E_FAIL;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_MONSTER);
	}

	return S_OK;
}

