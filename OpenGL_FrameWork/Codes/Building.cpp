#include "pch.h"
#include "..\Headers\Building.h"
#include "Buffer_CubeTex.h"
#include "Shader.h"
#include "Transform.h"


CBuilding::CBuilding()
{
}


CBuilding::~CBuilding()
{
	Release();
}

HRESULT CBuilding::Init()
{
	return E_NOTIMPL;
}

HRESULT CBuilding::Init(vec3 vPos, vec3 vSize, vec3 vColor)
{
	m_pBuffer = new CBuffer_CubeTex;
	m_pTransform = new CTransform;
	m_pBuffer->Init();
	m_pTransform->SetUp_Speed(0.5f, radians(5.f));
	m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	return S_OK;
}

int CBuilding::Update(const _float & fTimeDelta)
{
	return 0;
}

void CBuilding::Render()
{

	glUseProgram(CShaderMgr::GetInstance()->GetShader(L"Shader_CubeTexture")->GetShaderNum());


	mat4 matView = CCameraMgr::GetInstance()->Get_ViewMatrix();
	mat4 matProj = CCameraMgr::GetInstance()->Get_ProjMatrix();

	_uint WorldLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_CubeTexture")->GetShaderNum(), "matWorld");
	glUniformMatrix4fv(WorldLocation, 1, GL_FALSE, value_ptr(m_pTransform->GetMatrix()));

	_uint ViewLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_CubeTexture")->GetShaderNum(), "matView");
	glUniformMatrix4fv(ViewLocation, 1, GL_FALSE, &matView[0][0]);

	_uint ProjLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_CubeTexture")->GetShaderNum(), "matProj");
	glUniformMatrix4fv(ProjLocation, 1, GL_FALSE, &matProj[0][0]);

	_uint TextureLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_CubeTexture")->GetShaderNum(), "outTexture");
	glUniform1i(TextureLocation, 0);

	dynamic_cast<CBuffer_CubeTex*>(m_pBuffer)->Render(L"Texture_Building");
}

void CBuilding::Release()
{
	SafeDelete(m_pBuffer);
	SafeDelete(m_pTransform);
}
