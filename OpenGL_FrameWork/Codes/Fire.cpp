#include "pch.h"
#include "Fire.h"
#include "Buffer_RectTex.h"
#include "Shader.h"
#include "Transform.h"
#include "Texture.h"

CFire::CFire()
{
}

CFire::~CFire()
{
	Release();
}

HRESULT CFire::Init()
{
	return E_NOTIMPL;
}

HRESULT CFire::Init(vec3 vPos, vec3 vSize, _int iIdx)
{
	m_pBuffer = new CBuffer_RectTex;
	m_pTransform = new CTransform;
	m_pBuffer->Init();
	m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	m_pTransform->SetUp_Speed(0.002f, radians(5.f));
	m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Fire");
	m_pTransform->Scaling(vSize);
	m_tFrame = { 0,(_float)m_vecTexture.size() };
	m_eCurDir = (DIR)iIdx;


	if (m_eCurDir == DIR_UP)
	{
		vPos.y += 0.5f;
		m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	}
	else if (m_eCurDir == DIR_DOWN)
	{
		vPos.y -= 0.5f;
		m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	}
	else if (m_eCurDir == DIR_LEFT)
	{
		vPos.x -= 0.5f;
		m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	}
	else if (m_eCurDir == DIR_RIGHT)
	{
		vPos.x += 0.5f;
		m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	} 
	m_eID = ID_EFFECT;
	return S_OK;
}

int CFire::Update(const _float& fTimeDelta)
{
	CSoundMgr::GetInstance()->PlayOnce(L"F_Fire.wav", CSoundMgr::SOUND_EFFECT);
	CObj::MoveFrame(fTimeDelta / 1000.f, m_IsDead);

	if (m_IsDead)
		return 1;
	return 0;
}

void CFire::Late_Update(const _float& fTimeDelta)
{
}

void CFire::Render()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glUseProgram(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum());

	mat4 matView = CCameraMgr::GetInstance()->Get_ViewMatrix();
	mat4 matProj = CCameraMgr::GetInstance()->Get_ProjMatrix();
	_uint WorldLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matWorld");
	glUniformMatrix4fv(WorldLocation, 1, GL_FALSE, value_ptr(m_pTransform->GetMatrix()));

	_uint ViewLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matView");
	glUniformMatrix4fv(ViewLocation, 1, GL_FALSE, &matView[0][0]);

	_uint ProjLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matProj");
	glUniformMatrix4fv(ProjLocation, 1, GL_FALSE, &matProj[0][0]);

	_uint TextureLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "outTexture");
	glUniform1i(TextureLocation, 0);



	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_vecTexture[m_tFrame.fFrame]);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	m_pBuffer->Render();
	glDisable(GL_BLEND);


	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void CFire::Release()
{
	SafeDelete(m_pBuffer);
	SafeDelete(m_pTransform);
}
