#include "pch.h"
#include "Terrain.h"
#include "Buffer_RectTex.h"
#include "Shader.h"
#include "Transform.h"
#include "Texture.h"

CTerrain::CTerrain()
{
}

CTerrain::~CTerrain()
{
	Release();
}

HRESULT CTerrain::Init()
{
	m_pBuffer = new CBuffer_RectTex;
	m_pTransform = new CTransform;
	m_pBuffer->Init();
	m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Map");
	m_tFrame = { 0, (_float)m_vecTexture.size() };
	m_pTransform->Scaling(vec3(20.f, 20.f,10.f));
	m_eID = ID_TERRAIN;
	return S_OK;
}

int CTerrain::Update(const _float& fTimeDelta)
{

	return 0;
}

void CTerrain::Late_Update(const _float& fTimeDelta)
{
}

void CTerrain::Render()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glUseProgram(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum());

	mat4 matView = CCameraMgr::GetInstance()->Get_ViewMatrix();
	mat4 matProj = CCameraMgr::GetInstance()->Get_ProjMatrix();
	_uint WorldLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matWorld");
	glUniformMatrix4fv(WorldLocation, 1, GL_FALSE, value_ptr(m_pTransform->GetMatrix()));

	_uint ViewLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matView");
	glUniformMatrix4fv(ViewLocation, 1, GL_FALSE, &matView[0][0]);

	_uint ProjLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matProj");
	glUniformMatrix4fv(ProjLocation, 1, GL_FALSE, &matProj[0][0]);

	_uint TextureLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "outTexture");
	glUniform1i(TextureLocation, 0);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_vecTexture[m_tFrame.fFrame]);
	m_iCurTextureIdx++;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	m_pBuffer->Render();
	glDisable(GL_BLEND);


	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void CTerrain::Release()
{
	SafeDelete(m_pBuffer);
	SafeDelete(m_pTransform);
}
