#include "pch.h"
#include "..\Headers\Rect.h"
#include "Buffer_RectCol.h"
#include "Buffer_RectTex.h"
#include "Shader.h"
#include "Transform.h"
#include "Texture.h"


CRect::CRect()
{
}


CRect::~CRect()
{
	Release();
}

HRESULT CRect::Init()
{
	m_pBuffer = new CBuffer_RectTex;
	m_pTransform = new CTransform;
	m_pBuffer->Init();
	m_pTransform->Scaling(vec3(3.f, 3.f, 3.f));
	return S_OK;
}

HRESULT CRect::Init(vec3 vPos, vec3 vSize, vec3 vColor)
{
	m_pBuffer = new CBuffer_RectTex;
	m_pTransform = new CTransform;

	m_pBuffer->Init();

	m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	m_pTransform->Scaling(vSize);

	return S_OK;
}

int CRect::Update(const _float& fTimeDelta)
{
	vec3		vRight, vUp, vLook;

	mat4 matView = CCameraMgr::GetInstance()->Get_ViewMatrix();

	matView = inverse(matView);

	vRight = *(vec3*)&matView[0][0] * m_pTransform->GetScale().x;
	vUp = *(vec3*)&matView[1][0] * m_pTransform->GetScale().y;
	vLook = *(vec3*)&matView[2][0] * m_pTransform->GetScale().z;

	m_pTransform->Set_StateInfo(STATE_RIGHT, &vRight);

	m_pTransform->Set_StateInfo(STATE_LOOK, &vLook);

	
	if (m_IsDead)
		return 1;


	return 0;
}

void CRect::Late_Update(const _float& fTimeDelta)
{
}

void CRect::Render()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glUseProgram(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum());

	mat4 matView = CCameraMgr::GetInstance()->Get_ViewMatrix();
	mat4 matProj = CCameraMgr::GetInstance()->Get_ProjMatrix();
	_uint WorldLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matWorld");
	glUniformMatrix4fv(WorldLocation, 1, GL_FALSE, value_ptr(m_pTransform->GetMatrix()));

	_uint ViewLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matView");
	glUniformMatrix4fv(ViewLocation, 1, GL_FALSE, &matView[0][0]);

	_uint ProjLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matProj");
	glUniformMatrix4fv(ProjLocation, 1, GL_FALSE, &matProj[0][0]);

	_uint TextureLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "outTexture");
	glUniform1i(TextureLocation, 0);


	m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Diablo_Death");
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_vecTexture[m_iCurTextureIdx]);
	m_iCurTextureIdx++;

	if (m_iCurTextureIdx >= m_vecTexture.size())
	{
		m_iCurTextureIdx = 0;
	}

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	m_pBuffer->Render();
	glDisable(GL_BLEND);	


	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void CRect::Release()
{
	SafeDelete(m_pBuffer);
	SafeDelete(m_pTransform);
}
