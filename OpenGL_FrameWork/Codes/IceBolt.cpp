#include "pch.h"
#include "IceBolt.h"
#include "Buffer_RectTex.h"
#include "Shader.h"
#include "Transform.h"
#include "Texture.h"
#include "IceHit0.h"


CIceBolt::CIceBolt()
{
}

CIceBolt::~CIceBolt()
{
	Release();
}

HRESULT CIceBolt::Init()
{
	m_pBuffer = new CBuffer_RectTex;
	m_pTransform = new CTransform;
	m_pBuffer->Init();
	//m_pTransform->Scaling(vec3(3.f, 3.f, 3.f));
	m_pTransform->SetUp_Speed(0.003f, radians(5.f));


	return S_OK;
}

HRESULT CIceBolt::Init(vec3 vPos, vec3 vSize, _int iIdx)
{
	m_pBuffer = new CBuffer_RectTex;
	m_pTransform = new CTransform;
	m_pBuffer->Init();
	m_pTransform->Set_StateInfo(STATE_POSITION, &vPos);
	m_pTransform->SetUp_Speed(1.f, radians(5.f));

	m_eCurDir = (DIR)iIdx;

	if (m_eCurDir == DIR_UP)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Ice_Bolt0");
	}
	else if (m_eCurDir == DIR_DOWN)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Ice_Bolt8");
	}
	else if (m_eCurDir == DIR_LEFT)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Ice_Bolt12");
	}
	else if (m_eCurDir == DIR_RIGHT)
	{
		m_vecTexture = CTextureMgr::GetInstance()->GetTexture(L"Ice_Bolt4");
	}
	m_tFrame = { 0, (_float)m_vecTexture.size() };
	m_eID = ID_EFFECT;
	return S_OK;

}

int CIceBolt::Update(const _float& fTimeDelta)
{
	
	m_fLifeTime += fTimeDelta/1000.f;
	if (m_fLifeTime >= 2.f)
		m_IsDead = true;
	
	if (m_IsHit)
	{
		m_IsDead = true;
		CObj* pObj = CAbstractFactory<CIceHit0>::CreateObj(*m_pTransform->Get_StateInfo(STATE_POSITION), vec3(1.f, 1.f, 1.f), 0);
		if (nullptr == pObj)
			return 0;
		CObjectMgr::GetInstance()->Add_Object(pObj, OBJECT_EFFECT);
	}
	else
	{
		if (m_eCurDir == DIR_UP)
		{
			m_pTransform->Go_Up(fTimeDelta / 1000.f);
		}
		else if (m_eCurDir == DIR_DOWN)
		{ 
			m_pTransform->Go_Down(fTimeDelta / 1000.f);
		}
		else if (m_eCurDir == DIR_LEFT)
		{
			m_pTransform->Go_Left(fTimeDelta / 1000.f);
		}
		else if (m_eCurDir == DIR_RIGHT)
		{
			m_pTransform->Go_Right(fTimeDelta / 1000.f);
		}
	
	}


 	CObj::MoveFrame(fTimeDelta / 1000.f);


	if (m_IsDead)
		return 1;



	return 0;
}

void CIceBolt::Late_Update(const _float& fTimeDelta)
{
}

void CIceBolt::Render()
{
	glUseProgram(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum());

	mat4 matView = CCameraMgr::GetInstance()->Get_ViewMatrix();
	mat4 matProj = CCameraMgr::GetInstance()->Get_ProjMatrix();
	_uint WorldLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matWorld");
	glUniformMatrix4fv(WorldLocation, 1, GL_FALSE, value_ptr(m_pTransform->GetMatrix()));

	_uint ViewLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matView");
	glUniformMatrix4fv(ViewLocation, 1, GL_FALSE, &matView[0][0]);

	_uint ProjLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "matProj");
	glUniformMatrix4fv(ProjLocation, 1, GL_FALSE, &matProj[0][0]);

	_uint TextureLocation = glGetUniformLocation(CShaderMgr::GetInstance()->GetShader(L"Shader_Texture")->GetShaderNum(), "outTexture");
	glUniform1i(TextureLocation, 0);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_vecTexture[m_tFrame.fFrame]);
	//m_iCurTextureIdx++;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	m_pBuffer->Render();


	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void CIceBolt::Release()
{	SafeDelete(m_pBuffer);
	SafeDelete(m_pTransform);
}
