#include "pch.h"
#include "..\Headers\Texture.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

CTexture::CTexture()
{
}


CTexture::~CTexture()
{
}

HRESULT CTexture::Create_Texture(const char* pFilePath, GLuint& iIdx)
{
	std::vector<unsigned char> image;
	unsigned width, height;
	unsigned error = lodepng::decode(image, width, height, pFilePath);
	if (error != 0)
	{
		cout << "PNG Image Loading Failed : " << pFilePath << endl;
		assert(0);
	}

	glGenTextures(1, &iIdx);

	glBindTexture(GL_TEXTURE_2D, iIdx);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

	return S_OK;
}

_int CTexture::Find_Texture(const char* pFilePath)
{
	return _int();
}
