#include "pch.h"
#include "..\Headers\TextureMgr.h"
#include "Texture.h"


_IMPLEMENT_SINGLETON(CTextureMgr)
CTextureMgr::CTextureMgr()
{
}


CTextureMgr::~CTextureMgr()
{
	Release();
}

HRESULT CTextureMgr::Add_Texture(const _tchar * pTextureTag, const char * pFilePath, _int iIdx)
{
	vector<GLuint>	vecTexture = {};
	if (vecTexture.size() != 0)
		return E_FAIL;
	
	CTexture* pTexture = nullptr;
	for (int i = 0; i < iIdx; ++i)
	{		
		char	szFilePath[1024] = {};

		sprintf_s(szFilePath, pFilePath, i);
		GLuint iTemp=0;
		pTexture = new CTexture;
		if (FAILED(pTexture->Create_Texture(szFilePath, iTemp)))
		{
			SafeDelete(pTexture); 
			return E_FAIL;
		}
		SafeDelete(pTexture);
		vecTexture.push_back(iTemp);
	}

	m_mapTexture.insert({ pTextureTag, vecTexture });

	return S_OK;
}

vector<_uint> CTextureMgr::GetTexture(const _tchar * pTextureTag)
{
	vector<_uint> vecTexture = Find_Texture(pTextureTag);
	if (vecTexture.size() == 0)
		return vector<_uint>();

	return vecTexture;
}
vector<_uint> CTextureMgr::Find_Texture(const _tchar * pTextureTag)
{
	auto iter_find = find_if(m_mapTexture.begin(), m_mapTexture.end(), CFinder_Tag(pTextureTag));

	if (iter_find == m_mapTexture.end())
		return vector<_uint>();

	return (*iter_find).second;
}

void CTextureMgr::Release()
{

}
