#version 330 core
out vec4 FragColor;
 
in vec3 OutNormal;
in vec4 OutPos;
uniform	vec3	vLightPos;
uniform	vec3	vLightColor;

in vec2 TexCoord;

uniform sampler2D outTexture;

void main()
{
	vec3	vNormal = OutNormal;
	vec4	vLightDir = normalize( OutPos-vec4(vLightPos,1.0) );
	float	fDiffuseLight = max(dot(vec4(vNormal,1.0), vLightDir), 0.0);

	vec3	fDiffuse = fDiffuseLight * vLightColor;
	vec4	Text=texture(outTexture, TexCoord*30.f);

	vec4	vResult	= vec4(fDiffuse,1.f)*Text;

    FragColor = vResult;
} 