#version 330 core
out vec4 FragColor;
  

in vec2 TexCoord;

uniform sampler2D outTexture;

void main()
{
    vec4 OutColor = texture(outTexture, TexCoord);
    if(OutColor.x==0 && OutColor.y==0&& OutColor.z==0)
        OutColor.a=0;
    FragColor = OutColor;
} 