#version 330 core
layout (location = 0) in vec3 vPos;

out vec3 TexCoords;

uniform mat4 matWorld;
uniform mat4 matProj;
uniform mat4 matView;

void main()
{
    TexCoords = vPos;
    gl_Position = matProj * matView *matWorld* vec4(vPos, 1.0);
}  