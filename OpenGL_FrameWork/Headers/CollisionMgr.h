#pragma once
class CObj;
class CCollisionMgr
{
	_DECLARE_SINGLETON(CCollisionMgr)
private:
	CCollisionMgr();
	~CCollisionMgr();
public:
	_bool		Check_Collision(vector<CObj*> pSourLst, vector<CObj*> pDestLst, float fTemp=1.f);
};

