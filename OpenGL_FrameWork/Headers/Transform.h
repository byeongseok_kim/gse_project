#pragma once
class CObj;
class CTransform
{
	
public:
	CTransform();
	~CTransform();
public:
	HRESULT					Init();
public:
public:
	vec3*					Get_StateInfo(STATE eState);
	mat4					GetMatrix() { return m_matWorld; }
	mat4					GetNoneScaleMatrix( _float fScaleX, _float fScaleY, _float fScaleZ);
	vec3					GetScale();
	const mat4*				Get_Matrix_Pointer()const { return &m_matWorld; }
	mat4					Get_Matrix_Inverse()const;
public:
	void					SetMatrix(mat4 mat) { m_matWorld = mat; }
public:
	void					Set_StateInfo(STATE eState, const vec3* pInfo);
	void					Set_PositionY(const _float& Y) { m_matWorld[1][3] = Y; }
public:
	void					SetUp_Speed(const _float& fSpeed, const _float& fRotSpeed);

	void					Go_Straight(const _float& fTimeDelta);
	void					BackWard(const _float& fTimeDelta);
	void					Go_Left(const _float& fTimeDelta);
	void					Go_Right(const _float& fTimeDelta);
	void					Go_Up(const _float& fTimeDelta);
	void					Go_Down(const _float& fTimeDelta);

	void					SetUp_RotationX(const _float& fRadian);
	void					SetUp_RotationY(const _float& fRadian);
	void					SetUp_RotationZ(const _float& fRadian);

	void					RotationX(const _float& fTimeDelta);
	void					RotationY(const _float& fTimeDelta);
	void					RotationZ(const _float& fTimeDelta);

	void					Rotation_Axis(const _float& fTimeDelta, const vec3* pAxis);
	void					Scaling(vec3 vScale);
	void					Go_ToTarget(vec3* pTargetPos, const _float& fTimeDelta);
	void					Rotaion_Rev(mat4 matParent, const vec3 * pAxis);
	void					Go_Parabolic(vec3 vP1, vec3 vP2, vec3 vP3, _float fT);
private:
	mat4					m_matWorld = mat4(1.f);
	_float					m_fSpeed = 0.f;
	_float					m_fRotation_Speed = 0.f;
private:
	_int					m_iCnt_Shake=1;
};

