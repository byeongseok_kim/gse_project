#pragma once
#include "Obj.h"
class CArmor :
    public CObj
{
public:
    CArmor();
    virtual ~CArmor();
public:
    virtual HRESULT     Init() override;
    virtual HRESULT     Init(vec3 vPos, vec3 vSize, _int iIdx) override;
    virtual int         Update(const _float& fTimeDelta) override;
    virtual void	    Late_Update(const _float& fTimeDelta);
    virtual void        Render() override;
    virtual void        Release() override;
private:
    _float              m_fLifeTime = 0.f;
};

