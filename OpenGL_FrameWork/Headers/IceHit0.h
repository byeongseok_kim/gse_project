#pragma once
#include "Obj.h"
class CIceHit0 :
    public CObj
{
public:
    CIceHit0();
    virtual ~CIceHit0();
public:
    virtual HRESULT Init() override;
    virtual HRESULT Init(vec3 vPos, vec3 vSize, _int iIdx)override;
    virtual int Update(const _float& fTimeDelta) override;
    virtual void	Late_Update(const _float& fTimeDelta);
    virtual void Render() override;
    virtual void Release() override;
};

