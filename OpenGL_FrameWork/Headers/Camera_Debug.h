#pragma once
#include "Camera.h"
class CCamera_Debug :
	public CCamera
{
public:
	CCamera_Debug();
	virtual ~CCamera_Debug();
public:
	virtual HRESULT Init() override;
	virtual int Update(const _float& fTimeDelta) override;
	virtual void	Late_Update(const _float& fTimeDelta);

	virtual void Render() override;
	virtual void Release() override;
};

