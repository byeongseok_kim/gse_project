#pragma once
class CTexture;
class CTextureMgr
{
	_DECLARE_SINGLETON(CTextureMgr)
private:
	CTextureMgr();
	~CTextureMgr();
public:
	HRESULT				Add_Texture(const _tchar* pTextureTag, const char* pFilePath, _int iIdx = 1);
	vector<_uint>		GetTexture(const _tchar* pTextureTag);

private:
	vector<_uint>		Find_Texture(const _tchar* pTextureTag);
	void				Release();
private:
	map<const _tchar*, vector<_uint>>	m_mapTexture;
	vector<CTexture*>						m_vecTexture;
};

