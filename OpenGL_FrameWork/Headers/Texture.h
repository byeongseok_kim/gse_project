#pragma once
class CTexture
{
public:
	CTexture();
	~CTexture();
public:
	HRESULT			Create_Texture(const char* pFilePath, GLuint& iIdx);
	_int			GetTextureNum() { return m_iTextureNum; }
	TEXTURESET		GetTexture() { return m_tTexture; }
public:
	_int			GetCubeTextureNum() { return m_iCubeMapNum; }
private:
	_int			Find_Texture(const char* pFilePath);
private:
	_int			m_iTextureNum = 0;
	_int			m_iCubeMapNum = 0;
	TEXTURESET		m_tTexture;
};

