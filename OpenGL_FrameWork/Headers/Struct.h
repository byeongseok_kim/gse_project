#pragma once
typedef struct tagInfo
{
	tagInfo() {}
	tagInfo(int att, int def, int maxHp, int maxExp, int Lv)
	{
		iAtt = att;
		iDef = def;
		iMaxHP = maxHp;
		iHp = iMaxHP;
		iMaxExp = maxExp;
		iExp = 0;
		iLevel = Lv;
	}
	int		iAtt = 0;
	int		iDef = 0;
	int		iHp = 0;
	int		iMaxHP = 0;
	int		iExp = 0;
	int		iMaxExp = 0;
	int		iLevel = 0;
}INFO;
typedef struct tagFrame
{
	float fFrame;
	float fMax;
}FRAME;

typedef struct tagVertex_Color
{
	vec3 vPos;
	vec3 vColor;
}VTXCOL;

typedef struct tagVertex_Normal
{
	vec3 vPos;
	vec3 vColor;
	vec3 vNormal;
}VTXNORMAL;

typedef struct tagVertex_UV
{
	vec3 vPos;
	vec3 vNormal;
	vec2 vTexUV;
}VTXUV;

typedef struct tagCubeTexture
{
	vec3 vPos;
	vec3 vNormal;
	vec3 vTexUV;
}VTXCUBETEX;

typedef struct tagCamera_Desc
{
	vec3		vEye; // 카메라의 위치.In.WorldSpace
	vec3		vAt; // 카메라가 바라보는 점.
	vec3		vAxisY; // 내 좌표계상에서의 y축벡터의 방향.
}CAMERADESC;

typedef struct tagProjection_Desc
{
	float		fFovY; // 내 카메라의 시야 범위.
	float		fAspect; // 내 윈도우의 가로, 세로 비율.
	float		fNear;
	float		fFar;
}PROJDESC;

typedef struct tagTextureSet
{
	int iWidth;
	int iHeight;
	int iChannel;
	unsigned char* pData;
	int		iIdx;
}TEXTURESET;
