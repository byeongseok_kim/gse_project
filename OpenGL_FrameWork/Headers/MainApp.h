#pragma once
class CMainApp
{
public:
	CMainApp();
	~CMainApp();
public:
	HRESULT Init();
	void	Update(const _float& fTimeDelta);
	void	Render();
	void	Release();
private:
	HRESULT	Create_Player_Texture();
	HRESULT	Create_Effect_Texture();
	HRESULT	Create_Demon_Texture();
	HRESULT	Create_Diablo_Texture();
	HRESULT	Create_Cow_Texture();
	HRESULT	Create_Demon_Skill_Texture();
	HRESULT	Create_Attack_Range_Texture();
	HRESULT	CReate_Map_Texture();
private:
	HRESULT	Random_Create_Monster();

};

