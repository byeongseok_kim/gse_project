#pragma once
#ifndef __SOUNDMGR_H__
#define __SOUNDMGR_H__
#include "fmod.h"
class CSoundMgr
{
	_DECLARE_SINGLETON(CSoundMgr)

public:
	enum SOUND_ID {
		SOUND_BGM = 0, SOUND_PLAYER = 100, SOUND_MONSTER = 200, SOUND_EFFECT = 300, SOUND_UI = 400,
		SOUND_MOVIE = 500, SOUND_ONCE = 600, SOUND_REPEAT = 700, SOUND_END = 800
	};
private:
	CSoundMgr(void);
	~CSoundMgr(void);

public:
	void Initialize(void);
	void Update(void);
	void PlaySoundID(const TCHAR* pSoundKey, SOUND_ID eID);
	void PlayOnce(const TCHAR* pSoundKey, SOUND_ID eID);
	void PlayRepeat(const TCHAR* pSoundKey, SOUND_ID eID);
	void PlayBGM(const TCHAR* pSoundKey);
	void StopSound(SOUND_ID eID);
	void StopAll(void);
	void Release(void);

private:
	void LoadSoundFile(const char* pFilePath);

private:
	FMOD_SYSTEM*	m_pSystem;
	FMOD_CHANNEL*	m_pChannelArr[SOUND_END];
	
	map<const TCHAR*, FMOD_SOUND*>	m_MapSound;
	map<SOUND_ID, WORD>				m_MapCount;
};

#endif // !__SOUNDMGR_H__


