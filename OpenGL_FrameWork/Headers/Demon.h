#pragma once
#include "Obj.h"
class CDemon :
    public CObj
{
public:
    CDemon();
    virtual ~CDemon();
public:
    virtual HRESULT Init() override;
    virtual HRESULT Init(vec3 vPos, vec3 vSize, _int iIdx)override;
    virtual int Update(const _float& fTimeDelta) override;
    virtual void	Late_Update(const _float& fTimeDelta);
    virtual void Render() override;
    virtual void Release() override;
private:
    void            RandomDir();
    void            Movement(const _float& fTimeDelta);
    void            Movement(const vec3& vPlayerPos);
    void            Pattern(const _float& fTimeDelta);
    void            Attack();
private:
    _uint           m_iRandonDir = 0;
    _float          m_fSelectDirTime = 0.f;
    _float          m_fSkillTime = 0.f;
private:
    STATE           m_eState = END;
};

