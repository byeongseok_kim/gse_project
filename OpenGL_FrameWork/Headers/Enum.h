#pragma once
enum SHADER { SHADER_VERTEX, SHADER_FRAGMENT, SHADER_END };

enum OBJECT {OBJECT_UI, OBJECT_MONSTER, OBJECT_CAMERA, OBJECT_PLAYER, OBJECT_BULLET, OBJECT_EFFECT,OBJECT_MONSTER_BULLET, OBJECT_ATTACKRANGE, OBJECT_TERRAIN, OBJECT_END};

enum STATE { STATE_RIGHT, STATE_UP, STATE_LOOK, STATE_POSITION, STATE_END };

enum MOUSEBUTTON { DIM_LBUTTON, DIM_RBUTTON, DIM_WHEEL, DIM_XBUTTON };

enum MOUSEMOVE { DIM_X, DIM_Y, DIM_Z };

enum CAMERA {CAMERA_DEBUG, CAMERA_PLAYER, CAMERA_TOPVIEW, CAMERA_END};

enum TYPE_TEXTURE {TYPE_TEXTURE_RGB, TYPE_TEXTURE_RGBA, TYPE_TEXTURE_END};

enum LAYER_ID {ID_TERRAIN, ID_GAMEOBJECT, ID_EFFECT, ID_UI, ID_END};
