#pragma once
#include "Obj.h"
class CPlayer :
    public CObj
{
private:

public:
    CPlayer();
    virtual ~CPlayer();
public:
    virtual HRESULT     Init() override;
    virtual int         Update(const _float& fTimeDelta) override;
    virtual void	    Late_Update(const _float& fTimeDelta);
    virtual void        Render() override;
    virtual void        Release() override;
private:

    STATE        m_ePlayerState = END;

};

