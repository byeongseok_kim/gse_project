#pragma once
#include "Obj.h"
class CFireHit :
    public CObj
{
  
public:
    CFireHit();
    virtual ~CFireHit();
public:
    virtual HRESULT Init() override;
    virtual HRESULT Init(vec3 vPos, vec3 vSize, _int iIdx);
    virtual int Update(const _float& fTimeDelta) override;
    virtual void Late_Update(const _float& fTimeDelta) override;
    virtual void Render() override;
    virtual void Release() override;
};

