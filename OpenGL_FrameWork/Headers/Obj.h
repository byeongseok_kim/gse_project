#pragma once
class CBuffer;
class CTransform;
class CTexture;
class CObj
{
protected:
	enum DIR { DIR_UP, DIR_DOWN, DIR_LEFT, DIR_RIGHT, DIR_END };

	enum STATE {IDLE,  ATTACK, SKILL, DAMAGE, DEATH, END };
	DIR                 m_eCurDir = DIR_END;
	DIR                 m_ePreDir = DIR_END;
public:
	CObj();
	virtual ~CObj();
public:
	virtual HRESULT	Init()PURE;
	virtual HRESULT Init(vec3 vPos, vec3 vSize, vec3 vColor);
	virtual HRESULT	Init(vec3 vPos, vec3 vSize, vec3 vColor, CObj* pTarget);
	virtual HRESULT Init(vec3 vLightPos, vec3 vLightColor, string strLightName);
	virtual HRESULT Init(vec3 vP1, vec3 vP2, vec3 vP3, bool IsParabolic);
	virtual HRESULT Init(vec3 vPos, vec3 vSize, _int iIdx);
	virtual	int		Update(const _float& fTimeDelta)PURE;
	virtual void	Late_Update(const _float& fTimeDelta)PURE;
	virtual void	Render()PURE;
	virtual void	Release()PURE;
protected:
	void			MoveFrame(const _float& fTimeDelta);
	void			MoveFrame(const _float& fTimeDelta, _bool& IsDead);
	bool			MoveFrame_Once(const _float& fTimeDelta);

public:
	CTransform*		GetTransform() { return m_pTransform; }
	CBuffer*		GetBuffer() { return m_pBuffer; }
	LAYER_ID		Get_ID() { return m_eID; }
public:
	_bool&			GetIsDead() { return m_IsDead; }
	_bool&			GetIsHit() { return m_IsHit; }
protected:
	CBuffer*		m_pBuffer = nullptr;
	CTransform*		m_pTransform = nullptr;
protected:
	FRAME			m_tFrame = {};
	INFO			m_tInfo = {};
	LAYER_ID		m_eID = {};
protected:
	_float			m_fTimeDelta = 0.f;
protected:
	_bool			m_IsDead = false;
	_bool			m_IsHit = false;
public:
	void			Compute_RUL(vec3 vPos);
	void			Compute_RULO(vec3 vPos);
protected:
	vector<GLuint>	m_vecTexture;
	_uint			m_iCurTextureIdx = 0;
};

