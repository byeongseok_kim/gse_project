﻿
#ifndef PCH_H
#define PCH_H
#include <iostream>
#include "Dependencies/glew.h "
#include "Dependencies/freeglut.h"
#include "Dependencies/glm/glm.hpp"
#include "Dependencies/glm/ext.hpp"
#include "Dependencies/glm/gtc/matrix_transform.hpp"

#include <vector>
#include <list>
#include <map>
#include <string>
#include <fstream>
#include <algorithm>
#include <math.h>
#include <time.h>


using namespace std;
using namespace glm;

#define DIRECTINPUT_VERSION 0x0800
#include "dinput.h"

#include "Define.h"
#include "Constant.h"

#include "Function.h"
#include "Typedef.h"
#include "Struct.h"
#include "Enum.h"
#include "Extern.h"

#include "ObjectMgr.h"
#include "Abstract_Factory.h"
#include "ShaderMgr.h"
#include "KeyMgr.h"
#include "CameraMgr.h"
#include "InputMgr.h"
#include "TextureMgr.h"
#include "CollisionMgr.h"

#include "SoundMgr.h"
#include "LoadPng.h"

#include <io.h>
#include "fmod.h"
#pragma comment(lib, "fmodex_vc.lib")


#endif //PCH_H
